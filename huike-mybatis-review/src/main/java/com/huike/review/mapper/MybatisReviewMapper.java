package com.huike.review.mapper;

import com.huike.common.core.domain.AjaxResult;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {


    /**======================================================新增======================================================**/

    int saveData(@Param("name") String name,@Param("age") String age,@Param("sex") String sex);

    void saveDataJson(Review review);



    /**======================================================删除======================================================**/
    void remove(Long id);


    /**======================================================修改======================================================**/
    void updata(Review review);


    /**======================================================简单查询===================================================**/
    Review getById(Long id);


    int getTotalCount();
}
