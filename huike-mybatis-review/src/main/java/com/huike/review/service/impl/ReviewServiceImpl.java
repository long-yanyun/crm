package com.huike.review.service.impl;

import com.huike.common.core.domain.AjaxResult;
import com.huike.common.exception.CustomException;
import com.huike.common.utils.bean.BeanUtils;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.mapper.MybatisReviewMapper;
import com.huike.review.vo.MybatisReviewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * mybatis复习使用的业务层
 * 注意该业务层和我们系统的业务无关，主要是让同学们快速熟悉系统的
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private MybatisReviewMapper reviewMapper;

    /**=========================================================保存数据的方法=============================================*/
    @Override
    public Boolean saveData(String name, String age, String sex) {
        int i = reviewMapper.saveData(name, age, sex);
        if (i != 0){
            return true;
        }
        return false;
    }

    @Override
    public void saveData(MybatisReviewVO mybatisReviewVO) {
        Review review = new Review();
        BeanUtils.copyProperties(mybatisReviewVO,review);

        reviewMapper.saveDataJson(review);
    }

    /**=========================================================删除数据=============================================*/
    @Override
    public void remove(Long id) {
        reviewMapper.remove(id);
    }

    /**=========================================================修改数据=============================================*/
    @Override
    public void updata(MybatisReviewVO mybatisReviewVO) {
        Review review = new Review();
        BeanUtils.copyProperties(mybatisReviewVO,review);
        reviewMapper.updata(review);
    }

    /**=========================================================查询数据的方法=============================================*/
    @Override
    public Review getById(Long id) {
        Review byId = reviewMapper.getById(id);

        return byId;
    }

    @Override
    public void getDataByPage(Integer pageNum, Integer pageSize) {
        //查总条数
        reviewMapper.getTotalCount();

        /*当前页数-1）*每页条数*/

    }


}
