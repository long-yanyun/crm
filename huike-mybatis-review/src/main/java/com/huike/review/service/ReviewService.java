package com.huike.review.service;

import com.huike.common.core.domain.AjaxResult;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {


    Boolean saveData(String name,String age,String sex);

    void saveData(MybatisReviewVO mybatisReviewVO);

    void remove(Long id);

    void updata(MybatisReviewVO mybatisReviewVO);

    Review getById(Long id);

    void getDataByPage(Integer pageNum, Integer pageSize);
}
