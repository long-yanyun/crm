package com.huike.web.controller.review;


import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.vo.MybatisReviewVO;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.loadtime.Aj;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
//import sun.jvm.hotspot.debugger.Page;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
@Slf4j
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;

    /**=========================================================新增数据============================================*/
    /*//第三种传参方式：localhost:8080/employee/1493425203634528257 --> @PathVariable路径参数*/
    @GetMapping("/saveData/{name}/{age}/{sex}")
    public AjaxResult saveData(@PathVariable String name,
                               @PathVariable String age,
                               @PathVariable String sex){

        Boolean aBoolean = reviewService.saveData(name,age,sex);
        if (aBoolean){
            return AjaxResult.success();

        }
        return AjaxResult.error();
    }

    @PostMapping("/saveData")
    public AjaxResult saveData(@RequestBody MybatisReviewVO mybatisReviewVO){
        reviewService.saveData(mybatisReviewVO);
        return AjaxResult.success();
    }

    /**=========================================================删除数据=============================================*/

    @DeleteMapping("/remove/{id}")
    public AjaxResult remove(@PathVariable Long id){
        log.info("remove:{}",id);
        reviewService.remove(id);
        return AjaxResult.success();
    }

    /**=========================================================修改数据=============================================*/

    @PostMapping("/update")
    public AjaxResult update(@RequestBody MybatisReviewVO mybatisReviewVO){
        reviewService.updata(mybatisReviewVO);
        return AjaxResult.success();
    }

    /**=========================================================查询数据=============================================*/

    @GetMapping("/getById")
    public AjaxResult getById(Long id){
        Review review = reviewService.getById(id);

        return AjaxResult.success(review);
    }

    @GetMapping("/getDataByPage")
    public AjaxResult getDataByPage(Integer pageNum,Integer pageSize){
        reviewService.getDataByPage(pageNum,pageSize);
        return AjaxResult.success();
    }

}