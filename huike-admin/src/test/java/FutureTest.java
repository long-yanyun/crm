import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

public class FutureTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*并发异步编程CompletableFuture*/
        CompletableFuture<Integer> task1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("新线程1");
            try {
                System.out.println("等等我，还需要2秒时间...");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程1任务执行完毕");
            return 1;
        });

        CompletableFuture<Integer> task2 = CompletableFuture.supplyAsync(new Supplier() {
            @Override
            public Object get() {
                System.out.println("新线程2");
                return 2;
            }
        });

        CompletableFuture<Integer> task3 = CompletableFuture.supplyAsync(() -> {
            System.out.println("新线程3");
            return 3;
        });

        System.out.println("主线程执行...");
        //join()：阻塞主线程，等待所有的任务都执行完毕
        CompletableFuture.allOf(task1, task2, task3).join();
        //CompletableFuture.allOf(task1, task2, task3);
        System.out.println("做其他事了啊");

        System.out.println("线程1任务结果：" + task1.get());
        System.out.println("线程2任务结果：" + task2.get());
        System.out.println("线程3任务结果：" + task3.get());
        System.out.println("主线程结束");
    }
}
