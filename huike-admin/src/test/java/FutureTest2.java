import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

public class FutureTest2 {
    public static void main(String[] args) {
        int[] result = new int[3];

        CompletableFuture<Void> task1 = CompletableFuture.runAsync(() -> {
            System.out.println("新线程1");
            try {
                System.out.println("等等我，还需要2秒时间...");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result[0] = 1;
            System.out.println("线程1任务执行完毕");
        });

        CompletableFuture<Void> task2 = CompletableFuture.runAsync(() -> {
            System.out.println("新线程2");
            result[1] = 2;
        });

        CompletableFuture<Void> task3 = CompletableFuture.runAsync(() -> {
            System.out.println("新线程3");
            result[2] = 3;
        });

        CompletableFuture.allOf(task1, task2, task3).join();
        System.out.println(Arrays.toString(result));
    }
}
