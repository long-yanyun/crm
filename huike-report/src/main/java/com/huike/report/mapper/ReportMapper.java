package com.huike.report.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);


	/**=========================================今日简报========================================*/
	/*今日线索*/
	Integer getTodayCluesNum(@Param("username") String username,@Param("now") String now);

	Integer getTodayBusinessNum(@Param("username") String username,@Param("now") String now);

	Integer getTodayContractNum(@Param("username") String username,@Param("now") String now);

	Double getTodaySalesAmount(@Param("username") String username,@Param("now") String now);

	/**=========================================代办========================================*/


	Integer getTofollowedCluesNum(@Param("beginCreateTime") String beginCreateTime,
								  @Param("endCreateTime") String endCreateTime,
								  @Param("username") String username);

	Integer getTofollowedBusinessNum(@Param("beginCreateTime") String beginCreateTime,
									 @Param("endCreateTime") String endCreateTime,
									 @Param("username") String username);

	Integer getToallocatedCluesNum(@Param("beginCreateTime") String beginCreateTime,
								   @Param("endCreateTime") String endCreateTime,
								   @Param("username") String username);

	Integer getToallocatedBusinessNum(@Param("beginCreateTime") String beginCreateTime,
									  @Param("endCreateTime") String endCreateTime,
									  @Param("username") String username);





	/*Integer getTodayCluesNum(@Param("now") String today,@Param("username") String username);

	Integer getTodayBusinessNum(@Param("now") String today,@Param("username") String username);

	*//**
	 * 首页--今日简报--今日合同数量
	 * @param today  今日
	 * @param username 用户名
	 * @return
	 *//*
	Integer getTodayContractNum(@Param("now") String today,@Param("username") String username);

	*//**
	 * 首页--今日简报--今日销售金额
	 * @param today  今日
	 * @param username 用户名
	 * @return
	 *//*
	Double getTodaySalesAmount(@Param("now") String today,@Param("username") String username);*/

	/**=========================================待办========================================*/

}
