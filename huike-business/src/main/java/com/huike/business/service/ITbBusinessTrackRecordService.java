package com.huike.business.service;

import com.huike.business.domain.vo.BusinessTrackVo;

/**
 * 商机跟进记录Service接口
 * @date 2021-04-28
 */
public interface ITbBusinessTrackRecordService {


    void insert(BusinessTrackVo businessTrackVo);
}
