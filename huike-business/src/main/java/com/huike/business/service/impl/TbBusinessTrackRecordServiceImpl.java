package com.huike.business.service.impl;


import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商机跟进记录Service业务层处理
 * 
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {

    @Autowired
    TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;

    /*新增商机跟进记录*/
    @Override
    public void insert(BusinessTrackVo businessTrackVo) {
        tbBusinessTrackRecordMapper.insert(businessTrackVo);
    }
}
